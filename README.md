[![pipeline status](https://gitlab.com/Chedi/resume/badges/master/pipeline.svg)](https://gitlab.com/Chedi/resume/-/commits/master)
[![coverage report](https://gitlab.com/Chedi/resume/badges/master/coverage.svg)](https://gitlab.com/Chedi/resume/-/commits/master)

# resume

Just my resume but with a twist


Building
========
I'm using xelatex, but you can use luatex or any other compatible processor

    xelatex \\nonstopmode\\input cv_eng.tex

Embedding the fonts in the pdf
==============================
Tweak for the following command for your specific fonts paths:

    gs -sFONTPATH=/usr/share/fonts/:~/.local/share/fonts \
        -o cv_eng_with_fonts.pdf \
        -sDEVICE=pdfwrite         \
        -dPDFSETTINGS=/prepress  \
        cv_eng.pdf